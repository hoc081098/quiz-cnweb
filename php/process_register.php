<?php
/**
 * Created by PhpStorm.
 * User: Peter Hoc
 * Date: 12/17/2018
 * Time: 8:51 PM
 */

include_once 'check_logged_in_and_role.php';
include_once 'config/connect_db.php';

if (!isset($_POST['username']) || strlen($_POST['username']) < 3 || !isset($_POST['password']) || strlen($_POST['password']) < 6
    || !isset($_POST['birthday']) || !isset($_POST['full_name']) || !isset($_POST['class'])) {
    header('HTTP/1.0 422 Unprocessable Entity');
    echo json_encode(
        ['message' => 'Các trường không được rỗng'], JSON_UNESCAPED_UNICODE
    );
    exit();
}

$username = $_POST['username'];
$password = $_POST['password'];
$birthday = $_POST['birthday'];
$fullName = $_POST['full_name'];
$class = $_POST['class'];
$msg = isset($_POST['added_by_admin']) && $_POST['added_by_admin'] === 'true' ? 'Thêm user' : 'Đăng kí';

try {
    $pdo->beginTransaction();

    $getStmt = $pdo->prepare('SELECT name FROM users WHERE name = ? LIMIT 1');
    $getStmt->execute([$username]);
    if ($getStmt->fetch()) {
        header('HTTP/1.0 409 Conflict');
        echo json_encode(
            ['message' => 'Username đã được đăng kí'], JSON_UNESCAPED_UNICODE
        );
        exit();
    }

    $insertUserStmt = $pdo->prepare('INSERT INTO users(name, hashed_password, is_admin, created_at, updated_at) VALUES (?, ?, FALSE, NOW(), NOW())');
    $insertUserStmt->execute([$username, password_hash($password, PASSWORD_DEFAULT)]);

    $insertDetailStmt = $pdo->prepare("INSERT INTO userdetails(name, full_name, class, birthday) VALUES (?, ?, ?,  STR_TO_DATE(?, '%Y-%m-%d'))");
    $insertDetailStmt->execute([$username, $fullName, $class, $birthday]);

    $pdo->commit();

    header('HTTP/1.1 200 OK');
    echo json_encode(
        ['message' => $msg . ' thành công'],
        JSON_UNESCAPED_UNICODE
    );
    exit();

} catch (Exception $e) {
    $pdo->rollback();

    header('HTTP/1.0 500 Internal Server Error');
    echo json_encode(
        [
            'message' => "{$msg} thất bại"
        ], JSON_UNESCAPED_UNICODE
    );
    exit();
}
