<?php
/**
 * Created by PhpStorm.
 * User: Peter Hoc
 * Date: 12/19/2018
 * Time: 2:24 PM
 */

include_once 'check_logged_in_and_role.php';
include_once 'const.php';
include_once 'config/connect_db.php';

date_default_timezone_set('Asia/Ho_Chi_Minh');
if (!is_admin()) {
    header('Location: ' . ($_SERVER['HTTP_REFERER'] ?: 'index.php'));
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Search questions</title>

    <link href="../css/bootstrap.min.css" rel="stylesheet"
          type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../css/toast.css" type="text/css">


    <script src="../js/jquery-3.3.1.js"></script>
    <script src="../js/bootstrap.min.js"></script>


    <style type="text/css">
        body {
            padding: 80px 0;
        }

        .table > tbody > tr > td {
            vertical-align: middle;
        }

        .btn-circle {
            position: fixed;
            right: 30px;
            bottom: 100px;
            width: 56px;
            height: 56px;
            text-align: center;
            padding: 6px 0;
            font-size: 12px;
            line-height: 1.428571429;
            border-radius: 28px;
            box-shadow: 0 8px 12px 0 rgba(0, 0, 0, 0.2), 0 10px 24px 0 rgba(0, 0, 0, 0.19);
        }
    </style>

</head>

<body>

<?php include 'navbar.php' ?>

<div class="container">
    <div class="row justify-content-center">
        <form action="manage_questions.php" method="get" class="w-75">
            <div class="form-group">
                <label for="search-question-input">Tìm kiếm câu hỏi</label>
                <input type="text" class="form-control" id="search-question-input"
                       name="search-key" placeholder="" required autofocus>
            </div>
            <div class="form-group">
                <button type="submit"
                        name="submit"
                        value="submit"
                        class="form-control btn btn-primary" id="search-question-button">Search
                </button>
            </div>
        </form>
    </div>
</div>

<?php include 'footer.php' ?>

<script>
    $(() => {
        $('ul#ul-nav').find('.active').each(function () {
            $(this).removeClass('active');
        });
        $('#li-nav-manage').addClass('active');
        $('#li-nav-search-questions').addClass('active');
    });
</script>

</body>
</html>