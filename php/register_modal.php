<?php
/**
 * Created by PhpStorm.
 * User: Peter Hoc
 * Date: 12/15/2018
 * Time: 10:11 PM
 */
?>

<!-- Register Modal -->
<div class="modal fade" id="registerModal" role="dialog"
     aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Đăng kí</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="register-username">Username <small>(ít nhất 3 kí tự)</small></label>
                        <input type="text" class="form-control" id="register-username"
                               name="username" placeholder="Enter username"
                               required autofocus>
                    </div>

                    <div class="form-group">
                        <label for="register-fullname">Họ tên</label>
                        <input type="text" class="form-control" id="register-fullname"
                               name="fullname" placeholder="Enter full name"
                               required autofocus>
                    </div>

                    <div class="form-group">
                        <label for="register-class">Lớp</label>
                        <input type="text" class="form-control" id="register-class"
                               name="class" placeholder="Enter class"
                               required autofocus>
                    </div>

                    <div class="form-group">
                        <label for="register-birthday">Ngày sinh</label>
                        <input type="date" class="form-control" id="register-birthday"
                               name="birthday" min="1995-01-01">
                    </div>

                    <div class="form-group">
                        <label for="register-password">Password <small>(ít nhất 6 kí tự)</small></label>
                        <input name="password"
                               type="password" class="form-control"
                               id="register-password" placeholder="Password"
                               required>
                    </div>

                    <div class="row justify-content-center text-center p-0 m-0">
                        <span class=" alert " style="display: none;" id="register-msg"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                </button>
                <button type="submit" id="register-submit" name="login-submit"
                        class="btn btn-primary">Đăng kí
                </button>
            </div>
        </div>
    </div>
</div>
<!-- End Register Modal -->

<script>

    $(document).ready(() => {
        const performRegister = () => {
            const registerMsg = $('#register-msg');
            const username = $('#register-username').val();
            const password = $('#register-password').val();
            const fullName = $('#register-fullname').val();
            const class_ = $('#register-class').val();
            const birthday = $('#register-birthday').val();

            let isValid = true;
            if (!username || username.length < 3) {
                isValid = false;
            }
            if (!password || password.length < 6) {
                isValid = false;
            }
            if (!fullName) {
                isValid = false;
            }
            if (!class_) {
                isValid = false;
            }
            if (!birthday) {
                isValid = false;
            }

            if (!isValid) {
                registerMsg
                    .removeClass('alert-primary')
                    .removeClass('alert-success')
                    .addClass('alert-warning')
                    .stop(true, true)
                    .text('Hãy nhập đủ thông tin!')
                    .show()
                    .fadeOut(2000);
                return;
            }


            registerMsg
                .addClass('alert-primary')
                .removeClass('alert-success')
                .removeClass('alert-warning')
                .stop(true, true)
                .text('Loading...')
                .show();

            $.ajax({
                    url: 'process_register.php',
                    method: 'POST',
                    data: {
                        username,
                        password,
                        birthday,
                        full_name: fullName,
                        class: class_
                    },
                    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                    dataType: 'json',
                    success: data => {
                        console.log({data});

                        registerMsg
                            .removeClass('alert-primary')
                            .addClass('alert-success')
                            .removeClass('alert-warning')
                            .text(data['message'])
                            .stop(true, true)
                            .show()
                            .fadeOut(2000, () => {
                                $('#registerModal').modal('hide');
                                $('#loginModal').modal('show');
                            });
                    },
                    error: $xhr => {
                        registerMsg
                            .removeClass('alert-primary')
                            .removeClass('alert-success')
                            .addClass('alert-warning')
                            .text(JSON.parse($xhr.responseText)['message'])
                            .stop(true, true)
                            .show()
                            .fadeOut(2000);
                    }
                }
            );
        };

        $('#registerModal').find('input').each(function () {
            $(this).keypress(function (e) {
                if (e.which === 13) {
                    e.preventDefault();
                    performRegister();
                    return false;
                }
            });
        });

        $('#register-submit').click(function (e) {
            e.preventDefault();
            performRegister();
        });
    });
</script>
