<?php
/**
 * Created by PhpStorm.
 * User: Peter Hoc
 * Date: 12/14/2018
 * Time: 3:18 PM
 */

include_once 'check_logged_in_and_role.php';
include_once 'const.php';
include_once 'config/connect_db.php';

if (!is_admin()) {
    header('HTTP/1.0 403 Forbidden');
    echo json_encode(
        ['message' => 'Bạn phải là admin'], JSON_UNESCAPED_UNICODE
    );
    exit();
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if (!isset($_POST['content']) || !isset($_POST['a']) || !isset($_POST['b'])
        || !isset($_POST['c'])
        || !isset($_POST['d'])
        || !isset($_POST['answer'])
    ) {

        header('HTTP/1.0 405 Method Not Allowed');
        echo json_encode(
            ['message' => 'Các trường không được rỗng'], JSON_UNESCAPED_UNICODE
        );
        exit();

    }

    if (isset($_FILES['input-question-image'])
        && $_FILES['input-question-image']['error'] == 0
    ) {
        $ext = pathinfo(
            $_FILES['input-question-image']['name'], PATHINFO_EXTENSION
        );
        if (isset($ext)
            && !in_array(
                strtolower($ext), ['jpeg', 'jpg', 'png', 'gif', 'bmp']
            )
        ) {
            header('HTTP/1.0 405 Method Not Allowed');
            echo json_encode(
                ['message' => 'Định dạng ảnh cho phép: ' . join(
                        ['jpeg', 'jpg', 'png', 'gif', 'bmp'], ', '
                    )], JSON_UNESCAPED_UNICODE
            );
            exit();
        }
    }

    $stmtAdd = $pdo->prepare(
        'INSERT INTO questions(content, a, b, c, d, answer) VALUES (?, ?, ?, ?, ?, ?)'
    );
    if ($stmtAdd->execute(
        [
            $_POST['content'],
            $_POST['a'],
            $_POST['b'],
            $_POST['c'],
            $_POST['d'],
            $_POST['answer']
        ]
    )
    ) {
        $id = $pdo->lastInsertId();

        if (isset($_FILES['input-question-image'])
            && $_FILES['input-question-image']['error'] == 0
        ) {
            $ext = pathinfo(
                $_FILES['input-question-image']['name'], PATHINFO_EXTENSION
            );


            $newName = $id . '.' . $ext;
            $path = '../images/' . $newName;

            if (file_exists($path)) {
                unlink($path);
            }

            if (!move_uploaded_file(
                $_FILES['input-question-image']['tmp_name'], $path)
            ) {
                header('HTTP/1.0 201 Created');
                echo json_encode(
                    ['message' => 'Thêm thành công nhưng lỗi khi lưu hình ảnh'],
                    JSON_UNESCAPED_UNICODE
                );
                exit();
            }

            $updateStmt = $pdo->prepare(
                'UPDATE questions SET image = ? WHERE id = ?'
            );
            if ($updateStmt->execute([$path, $id])) {

                $getStmt = $pdo->prepare(
                    'SELECT * FROM questions WHERE id = ? LIMIT 1'
                );
                if ($getStmt->execute([$id]) && $question = $getStmt->fetch()) {

                    header('HTTP/1.0 201 Created');
                    echo json_encode(
                        [
                            'message' => 'Thêm câu hỏi thành công',
                            'added' => $question
                        ], JSON_UNESCAPED_UNICODE
                    );
                    exit();

                } else {

                    header('HTTP/1.0 201 Created');
                    echo json_encode(
                        [
                            'message' => 'Thêm câu hỏi thành công'],
                        JSON_UNESCAPED_UNICODE
                    );
                    exit();

                }
            } else {
                header('HTTP/1.0 500 Internal Server Error');
                echo json_encode(
                    [
                        'message' => 'Thêm câu hỏi thành công nhưng không update được hình ảnh'],
                    JSON_UNESCAPED_UNICODE
                );
                exit();
            }
        } else {
            $getStmt = $pdo->prepare(
                'SELECT * FROM questions WHERE id = ? LIMIT 1'
            );
            if ($getStmt->execute([$id]) && $question = $getStmt->fetch()) {

                header('HTTP/1.0 201 Created');
                echo json_encode(
                    [
                        'message' => 'Thêm câu hỏi thành công',
                        'added' => $question
                    ], JSON_UNESCAPED_UNICODE
                );
                exit();

            } else {

                header('HTTP/1.0 201 Created');
                echo json_encode(
                    [
                        'message' => 'Thêm câu hỏi thành công'],
                    JSON_UNESCAPED_UNICODE
                );
                exit();
            }
        }

    } else {
        header('HTTP/1.0 500 Internal Server Error');
        echo json_encode(
            ['message' => 'Lỗi khi thêm câu hỏi'], JSON_UNESCAPED_UNICODE
        );
        exit();
    }
}