<?php
/**
 * Created by PhpStorm.
 * User: Peter Hoc
 * Date: 12/11/2018
 * Time: 6:46 PM
 */

include_once 'check_logged_in_and_role.php';
include_once 'const.php';
include_once 'config/connect_db.php';

date_default_timezone_set('Asia/Ho_Chi_Minh');

if (!is_admin()) {
    header('Location: ' . ($_SERVER['HTTP_REFERER'] ?: 'index.php'));
    exit();
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Manage questions</title>

    <link href="../css/bootstrap.min.css" rel="stylesheet"
          type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="../css/toast.css" type="text/css">


    <script src="../js/jquery-3.3.1.js" crossorigin="anonymous">
    </script>

    <script src="../js/bootstrap.min.js"
            crossorigin="anonymous"></script>

    <style type="text/css">
        body {
            padding: 80px 0;
        }

        .table > tbody > tr > td {
            vertical-align: middle;
        }

        #table-body tr td li.active {
            background-color: rgba(167, 110, 203, 0.59);
            border: none;
        }

        .btn-circle {
            position: fixed;
            right: 30px;
            bottom: 100px;
            width: 56px;
            height: 56px;
            text-align: center;
            padding: 6px 0;
            font-size: 12px;
            line-height: 1.428571429;
            border-radius: 28px;
            box-shadow: 0 8px 12px 0 rgba(0, 0, 0, 0.2), 0 10px 24px 0 rgba(0, 0, 0, 0.19);
        }
    </style>


</head>

<body>

<?php include 'navbar.php' ?>

<div class="container">

    <?php if (isset($_GET['submit'])) : ?>
        <h5 class="text-center text-info m-4">Kết quả tìm kiếm '<?php echo $_GET['search-key'] ?>'</h5>
    <?php else : ?>
        <h5 class="text-center text-info m-4">Tất cả câu hỏi</h5>
    <?php endif; ?>

    <table class="table table-bordered text-center table-hover table-responsive-md">
        <thead>
        <tr class="bg-info text-white">
            <th scope="col">STT</th>
            <th scope="col">Nội dung</th>
            <th scope="col">Đáp án</th>
            <th scope="col">Chỉnh sửa</th>
            <th scope="col">Xóa</th>
        </tr>
        </thead>

        <tbody id="table-body">
            <?php include_once 'process_get_question.php'; ?>
        </tbody>

    </table>

    <div class="row text-center justify-content-center">
        <button id="load-more" class="btn btn-info px-4 py-3">Load more</button>
    </div>


</div>

<?php include 'footer.php' ?>


<!--Add button-->
<button type="button" class="btn btn-primary btn-circle"
        id="button-add-question" data-toggle="modal"
        data-target="#add-modal">
    <i class="material-icons" style="vertical-align: middle;">add</i>
</button>
<!--End Add button-->


<!-- Edit modal -->
<div class="modal fade" id="edit-modal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Chỉnh sửa câu
                    hỏi</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form action="" id="update-form" enctype="multipart/form-data">

                    <div class="form-group">
                        <label for="input-content">Nội dung câu hỏi</label>
                        <p>
                            <small id="created-at"></small>
                        </p>
                        <textarea class="form-control" id="input-content"
                                  rows="4" name="content"></textarea>
                    </div>


                    <div class="text-center">
                        <img id="question-image" src=""
                             class="rounded .img-thumbnail" alt=""
                             style="width: 100%;">
                        <button type="button" class="btn btn-danger m-4"
                                id="btn-delete-image">Xóa ảnh
                        </button>
                        <input id="input-upload-mage" type="file"
                               accept="image/*" name="input-question-image"
                               class="mx-auto mb-3"/>
                        <input type="hidden" name="original-image"
                               id="original-image">
                    </div>

                    <ul class="list-group list-group-flush">
                        <li class="list-group-item  ">
                            <div class="custom-control custom-radio custom-control-inline w-100">
                                <input type="radio"
                                       name="answer"
                                       id="a"
                                       value="a"
                                       class="custom-control-input">
                                <label class="custom-control-label"
                                       for="a">a</label>
                                <input type="text" class="form-control ml-4"
                                       name="a" id="input-answer-a">
                            </div>
                        </li>

                        <li class="list-group-item">
                            <div class="custom-control custom-radio custom-control-inline w-100">
                                <input type="radio"
                                       name="answer"
                                       value="b"
                                       id="b"
                                       class="custom-control-input">
                                <label class="custom-control-label"
                                       for="b">b</label>
                                <input type="text" class="form-control ml-4"
                                       name="b" id="input-answer-b">
                            </div>
                        </li>

                        <li class="list-group-item">
                            <div class="custom-control custom-radio custom-control-inline w-100">
                                <input type="radio"
                                       name="answer"
                                       id="c"
                                       value="c"
                                       class="custom-control-input">
                                <label class="custom-control-label"
                                       for="c">c</label>
                                <input type="text" class="form-control ml-4"
                                       name="c" id="input-answer-c">
                            </div>
                        </li>

                        <li class="list-group-item">
                            <div class="custom-control custom-radio custom-control-inline w-100">
                                <input type="radio"
                                       name="answer"
                                       id="d"
                                       value="d"
                                       class="custom-control-input">
                                <label class="custom-control-label"
                                       for="d">d</label>
                                <input type="text" class="form-control ml-4"
                                       name="d" id="input-answer-d">
                            </div>
                        </li>
                    </ul>

                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                        data-dismiss="modal">Close
                </button>
                <button type="button" class="btn btn-primary"
                        id="button-save-changes">Lưu thay đổi
                </button>
            </div>
        </div>
    </div>
</div>
<!--End edit modal-->


<!-- Add modal -->
<div class="modal fade" id="add-modal" role="dialog"
     aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Thêm câu hỏi</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form action="" id="add-form" enctype="multipart/form-data">

                    <div class="form-group">
                        <label for="input-content">Nội dung câu hỏi</label>
                        <textarea class="form-control" id="add-input-content"
                                  rows="4" name="content"></textarea>
                    </div>

                    <div class="text-center">
                        <img id="add-question-image" src=""
                             class="rounded .img-thumbnail" alt=""
                             style="width: 100%;">
                        <button type="button" class="btn btn-danger m-4"
                                id="add-btn-delete-image">Xóa ảnh
                        </button>
                        <input id="add-input-upload-mage" type="file"
                               accept="image/*" name="input-question-image"
                               class="mx-auto mb-3"/>
                    </div>

                    <ul class="list-group list-group-flush">
                        <li class="list-group-item  ">
                            <div class="custom-control custom-radio custom-control-inline w-100">
                                <input type="radio"
                                       name="answer"
                                       id="add-a"
                                       value="a"
                                       class="custom-control-input">
                                <label class="custom-control-label" for="add-a">a</label>
                                <input type="text" class="form-control ml-4"
                                       name="a" id="add-input-answer-a">
                            </div>
                        </li>

                        <li class="list-group-item">
                            <div class="custom-control custom-radio custom-control-inline w-100">
                                <input type="radio"
                                       name="answer"
                                       value="b"
                                       id="add-b"
                                       class="custom-control-input">
                                <label class="custom-control-label" for="add-b">b</label>
                                <input type="text" class="form-control ml-4"
                                       name="b" id="add-input-answer-b">
                            </div>
                        </li>

                        <li class="list-group-item">
                            <div class="custom-control custom-radio custom-control-inline w-100">
                                <input type="radio"
                                       name="answer"
                                       id="add-c"
                                       value="c"
                                       class="custom-control-input">
                                <label class="custom-control-label" for="add-c">c</label>
                                <input type="text" class="form-control ml-4"
                                       name="c" id="add-input-answer-c">
                            </div>
                        </li>

                        <li class="list-group-item">
                            <div class="custom-control custom-radio custom-control-inline w-100">
                                <input type="radio"
                                       name="answer"
                                       id="add-d"
                                       value="d"
                                       class="custom-control-input">
                                <label class="custom-control-label" for="add-d">d</label>
                                <input type="text" class="form-control ml-4"
                                       name="d" id="add-input-answer-d">
                            </div>
                        </li>
                    </ul>

                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                        data-dismiss="modal">Close
                </button>
                <button type="button" class="btn btn-primary"
                        id="modal-button-add-question">Thêm
                </button>
            </div>
        </div>
    </div>
</div>
<!--End Add modal-->


<!--SnackBar-->
<div id="snackbar">Some text some message..</div>
<!--End SnackBar-->


<?php include_once 'manage_question_js.php' ?>

</body>

</html>
