<?php
/**
 * Created by PhpStorm.
 * User: Peter Hoc
 * Date: 12/12/2018
 * Time: 8:23 PM
 */

include_once 'config/connect_db.php';
include_once 'check_logged_in_and_role.php';

$next = isset($_POST['next']) ? $_POST['next'] : (isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'index.php');

if (!is_admin() || !isset($_POST['name'])) {
    header('Location: ' . $next);
    exit();
}

$statement = $pdo->prepare('DELETE FROM users WHERE name = ?');
if ($statement->execute([$_POST['name']])) {
    $_SESSION['removed-user'] = 'success';
    header('Location: ' . ($next));
    exit();
} else {
    $_SESSION['removed-user'] = 'error';
    header('Location: ' . ($next));
    exit();
}