<?php
/**
 * Created by PhpStorm.
 * User: Peter Hoc
 * Date: 12/12/2018
 * Time: 2:48 PM
 */
?>


<!-- Footer -->
<footer class="py-4 bg-dark fixed-bottom">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; 2018</p>
    </div>
    <!-- /.container -->
</footer>
