<?php
/**
 * Created by PhpStorm.
 * User: Peter Hoc
 * Date: 12/15/2018
 * Time: 7:55 PM
 */

include_once 'check_logged_in_and_role.php';
include_once 'const.php';
include_once 'config/connect_db.php';

date_default_timezone_set('Asia/Ho_Chi_Minh');
if (!is_admin()) {
    header('Location: ' . ($_SERVER['HTTP_REFERER'] ?: 'index.php'));
    exit();
}

$results = $pdo->query(
    "SELECT ud.name, ud.full_name, ud.class, DATE_FORMAT(ud.birthday, '%d/%m/%Y') as birthday, SUM(score) as sum_score, SUM(elapsed_time) as sum_elapsed_time, COUNT(id) as count_test
         FROM userdetails ud INNER JOIN users u on ud.name = u.name LEFT JOIN details d on ud.name = d.name 
         GROUP BY ud.name
         ORDER BY ud.name ASC , created_at DESC 
         LIMIT " . LIMIT_HIGH_SCORE
);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Manage users</title>

    <link href="../css/bootstrap.min.css" rel="stylesheet"
          type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="../css/toast.css" type="text/css">


    <script src="../js/jquery-3.3.1.js" crossorigin="anonymous">
    </script>

    <script src="../js/bootstrap.min.js" crossorigin="anonymous">
    </script>

    <style type="text/css">
        body {
            padding: 80px 0;
        }

        .table > tbody > tr > td {
            vertical-align: middle;
        }

        .btn-circle {
            position: fixed;
            right: 30px;
            bottom: 100px;
            width: 56px;
            height: 56px;
            text-align: center;
            padding: 6px 0;
            font-size: 12px;
            line-height: 1.428571429;
            border-radius: 28px;
            box-shadow: 0 8px 12px 0 rgba(0, 0, 0, 0.2), 0 10px 24px 0 rgba(0, 0, 0, 0.19);
        }
    </style>

</head>

<body>

<?php include 'navbar.php' ?>

<div class="container">

    <?php

    if (isset($_SESSION['removed-user']) && $_SESSION['removed-user'] === 'error') {
        echo '<div class="row justify-content-center text-center p-0 m-0">
                    <span class="alert alert-warning removed-user-msg">Xóa user không thành công</span>
               </div>';
        unset($_SESSION['removed-user']);
    } elseif (isset($_SESSION['removed-user']) && $_SESSION['removed-user'] === 'success') {
        echo '<div class="row justify-content-center text-center p-0 m-0">
                    <span class="alert alert-success removed-user-msg">Xóa users thành công</span>
              </div>';
        unset($_SESSION['removed-user']);
    }
    ?>


    <table class="table table-bordered text-center table-hover table-responsive-md">
        <thead>
        <tr class="bg-primary text-white">
            <th scope="col">STT</th>
            <th scope="col">Username</th>
            <th scope="col">Họ tên</th>
            <th scope="col">Lớp</th>
            <th scope="col">Ngày sinh(dd/MM/yyyy)</th>
            <th scope="col">Tổng điểm</th>
            <th scope="col">Tổng thời gian(s)</th>
            <th scope="col">Số lần thi</th>
            <th scope="col">Chỉnh sửa</th>
            <th scope="col">Xóa</th>
        </tr>
        </thead>

        <tbody id="table-body">
        <?php foreach ($results as $index => $result) : ?>
            <tr>
                <td scope="row"
                    class="font-weight-bold"><?php echo str_pad(
                        $index + 1, 2, '0', STR_PAD_LEFT
                    ); ?></td>
                <td class="font-weight-bold"><a
                            href="detail.php?username=<?php echo $result['name']; ?>"><?php echo $result['name']; ?></a>
                </td>
                <td><?php echo $result['full_name']; ?></td>
                <td><?php echo $result['class']; ?></td>
                <td><?php echo $result['birthday']; ?></td>
                <td><?php echo $result['sum_score'] ?: 0; ?></td>
                <td><?php echo $result['sum_elapsed_time'] ?: 0; ?></td>
                <td><?php echo $result['count_test'] ?: 0; ?></td>
                <td>
                    <a class="btn btn-default "
                       href="edit_user.php?name=<?php echo $result['name']; ?>">
                        <i class="material-icons" style="vertical-align: middle;">edit</i>
                    </a>
                </td>
                <td>
                    <form action="process_delete_user.php" method="post"
                          class="form-inline">
                        <button class="btn btn-danger delete-user"
                                type="submit" value="">
                            <i class="material-icons"
                               style="vertical-align: middle;">delete</i>
                        </button>
                        <input type="hidden" name="name"
                               value="<?php echo $result['name']; ?>">
                    </form>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>

    </table>
</div>

<?php include 'footer.php' ?>

<?php include 'add_user_modal.php' ?>

<!--Add button-->
<button type="button" class="btn btn-primary btn-circle" data-toggle="modal" data-target="#addUserModal">
    <i class="material-icons">add</i>
</button>
<!--End Add button-->


<script>
    $(() => {
        $('ul#ul-nav').find('.active').each(function () {
            $(this).removeClass('active');
        });
        $('#li-nav-manage').addClass('active');
        $('#li-nav-manage-users').addClass('active');

        $('.delete-user').click(e => {
            let c;
            if (c = confirm('Bạn có chắc chắn muốn xóa user này không?')) {
                $('#delete-user-form').submit();
            }
            return c;
        });

        $('.removed-user-msg').fadeOut(2000, function () {
            $(this).remove();
        });
    });
</script>

</body>
</html>