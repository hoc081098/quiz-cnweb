<?php
/**
 * Created by PhpStorm.
 * User: Peter Hoc
 * Date: 12/11/2018
 * Time: 6:42 PM
 */

include_once 'check_logged_in_and_role.php';
include_once 'config/connect_db.php';
include_once 'const.php';

if (!is_user_logged_in()) {
    if (!is_user_logged_in()) {
        header('Location: index.php?open_login=true');
        exit();
    }
}

$questions = $pdo
    ->query(
        'SELECT * FROM questions ORDER BY RAND() LIMIT ' . MAX_NUMBER_QUESTION
    )
    ->fetchAll(PDO::FETCH_ASSOC);
$startTime = new DateTime();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test</title>


    <link href="../css/bootstrap.min.css" rel="stylesheet"
          type="text/css">


    <script src="../js/jquery-3.3.1.js" crossorigin="anonymous"></script>
    <script src="../js/bootstrap.min.js" crossorigin="anonymous"></script>



    <style type="text/css">
        body {
            padding: 80px 0px;
        }

        #timer {
            position: fixed;
            top: 50%;
            left: 40px;
            z-index: 99999;
            background-color: rgba(32, 168, 109, 0.62);
            padding: 12px;
            color: white;
            border-radius: 8px;
        }

        .list-group-item:hover{
            border: solid 1.5px #007bff;
        }
    </style>
</head>
<body>

<?php include 'navbar.php'; ?>

<!-- Page Content -->

<h4 id="timer">15:00</h4>

<div class="container">
    <div>
        <form action="check_test_result.php" method="post" id="test-form">
            <?php foreach ($questions as $index => $question) : ?>
                <div class="card mb-3">
                    <div class="card-body">

                        <h5 class="card-title"><?php echo ($index + 1) . '. '
                                . $question['content'] ?></h5>


                        <?php if (isset($question['image'])): ?>

                            <div class="w-100"
                                 style="text-align: center;vertical-align: middle; ">
                                <img class="card-img .img-thumbnail rounded m-4"
                                     style="width: 50%; height: 50%; left: 50%; right: 50%;"
                                     alt=""
                                     src="<?php echo $question['image'] . '?'
                                         . Date('U') ?>">
                            </div>
                        <?php endif; ?>
                        <ul class="list-group">

                            <?php foreach (range('a', 'd') as $ans): ?>

                                <li class="list-group-item">
                                    <div class="custom-control custom-radio w-100">
                                        <input type="radio"
                                               id="answer-<?php echo $question['id']
                                                   . $ans; ?>"
                                               name="<?php echo $question['id'] ?>"
                                               value="<?php echo $ans; ?>"
                                               class="custom-control-input">
                                        <label class="custom-control-label  w-100 p-1"
                                               style="cursor: pointer; "
                                               for="answer-<?php echo $question['id']
                                                   . $ans; ?>"><?php echo strtoupper(
                                                    $ans
                                                ) . '. '
                                                . $question[$ans]; ?></label>
                                    </div>
                                </li>


                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>

            <?php endforeach; ?>

            <input type="hidden" name="start-time" value="<?php echo $startTime->getTimestamp(); ?>">
            <input type="hidden" name="total-number" value="<?php echo count($questions); ?>">
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
<!-- /.container -->

<?php include 'footer.php'; ?>


<script>
    const toHHmm = time => Math.round(time / 60).toString().padStart(2, '0') + ':' + (time % 60).toString().padStart(2, '0')

    $(document).ready(function () {
        let time = 15 * 60;
        const interval = setInterval(() => {
            $('#timer').text(toHHmm(time));
            --time;
            if (time < 0) {
                clearInterval(interval);
                $('form').submit();
            }
        }, 1000);

        $('#test-form').submit(function (e) {
            $(window).unbind('beforeunload');
        });

        $('ul#ul-nav').find('.active').each(function () {
            $(this).removeClass('active');
        });
        $('#li-nav-test').addClass('active');
    });


    $(window).bind('beforeunload', function (e) {
        const message = "Bạn có muốn thoát bài thi ?";
        e.returnValue = message;
        return message;
    });


</script>

</body>
</html>
