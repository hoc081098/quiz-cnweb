<?php
/**
 * Created by PhpStorm.
 * User: Peter Hoc
 * Date: 12/19/2018
 * Time: 3:36 PM
 */
?>

<script>
    "use strict";


    let isLoading = false;
    let loadAllData = false;
    let currentPage = 1;

    function refreshAllPages() {
        const searchKey = "<?php echo !empty($_GET['search-key']) ? $_GET['search-key'] : '' ?>";
        const getData = {
            'page': currentPage,
            'search-key': searchKey,
            'load_all_page': true
        };

        $.ajax({
            type: 'GET',
            url: 'process_get_question.php',
            data: getData,
            dataType: 'html',
            success: data => {
                $('#table-body')
                    .fadeOut('slow', function () {
                        $(this).empty()
                            .append(data)
                            .show();
                    });
            },
            error: $xhr => {
            }
        });
    }

    $(document).ready(() => {
        $('ul#ul-nav').find('.active').each(function () {
            $(this).removeClass('active');
        });
        $('#li-nav-manage').addClass('active');
        $('#li-nav-manage-questions').addClass('active');

        /**
         * Load more
         */

        $('#load-more').click(function () {
            if (!isLoading && !loadAllData) {
                const button = $(this);
                const elements = $('#table-body');

                button.html('Loading...');
                isLoading = true;
                ++currentPage;

                const searchKey = "<?php echo !empty($_GET['search-key']) ? $_GET['search-key'] : '' ?>";
                const getData = {'page': currentPage, 'search-key': searchKey};

                $.ajax({
                    type: 'GET',
                    url: 'process_get_question.php',
                    data: getData,
                    dataType: 'html',
                    success: data => {
                        elements.append(data);
                        loadAllData = elements.children('input[type=hidden][name=load-all-data]').last().attr('value') === '1';
                        if (loadAllData) {
                            button.fadeOut('slow', () => button.remove());
                            const snackBar = $("#snackbar");
                            snackBar
                                .text('Load hết câu hỏi')
                                .addClass('show');
                            setTimeout(() => snackBar.removeClass('show'), 2000);
                        }
                    },
                    error: $xhr => {
                        const snackBar = $("#snackbar");
                        snackBar
                            .text('Lỗi khi load câu hỏi')
                            .addClass('show');
                        setTimeout(() => snackBar.removeClass('show'), 2000);
                    }
                }).always(() => {
                    button.html('Load more');
                    isLoading = false;
                });
            }
        });


        /**
         * Update question
         */

        let inputUploadImage = $('#input-upload-mage');
        const image = $('#question-image');

        $('#btn-delete-image').click(() => {
            image.fadeOut('fast');
            $('#original-image').val(null);
            inputUploadImage.val(null);
        });

        inputUploadImage.change((e) => {
            if (e.target.files && e.target.files[0]) {
                const reader = new FileReader();
                reader.onload = ev => image.fadeIn('fast').attr('src', ev.target.result)
                reader.readAsDataURL(e.target.files[0]);
            }
        });

        $('#edit-modal').on('show.bs.modal', function (event) {

            const button = $(event.relatedTarget);
            const id = button.attr('id');

            const editModal = $(this);
            const inputContent = $('#input-content');
            const answerA = $('#input-answer-a');
            const answerB = $('#input-answer-b');
            const answerC = $('#input-answer-c');
            const answerD = $('#input-answer-d');
            const createdAt = $('#created-at');

            inputContent.val('');
            image.fadeOut('fast');
            answerA.val('');
            answerB.val('');
            answerC.val('');
            answerD.val('');
            createdAt.text('');
            inputUploadImage.val(null);
            editModal.find('input:radio[name=answer]').each(function () {
                $(this).prop('checked', false);
            });

            console.log(id);

            $.ajax({
                type: 'GET',
                url: 'process_edit_question.php',
                data: {'question-id': id},
                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                dataType: 'json',
                success: data => {
                    const questionModel = data['question'];
                    inputContent.val(questionModel['content']);
                    if (questionModel['image']) {
                        image.fadeIn('fast').attr('src', questionModel['image'] + '?' + new Date().getTime())
                    }
                    $('#original-image').val(questionModel['image']);
                    answerA.val(questionModel['a']);
                    answerB.val(questionModel['b']);
                    answerC.val(questionModel['c']);
                    answerD.val(questionModel['d']);
                    createdAt.text(`Ngày tạo: ${questionModel['created_at']}`);
                    $(`input:radio[name=answer]#${questionModel['answer']}`).prop('checked', true);

                    $('#button-save-changes').off('click').on('click', () => {
                        const data = new FormData($('#update-form')[0]);
                        data.append('id', id);

                        console.log('Update...', id);

                        function updateListAnswer(curAnswer, li, updated) {
                            li.text(`${curAnswer.toUpperCase()}. ${updated[curAnswer]}`);
                            if (curAnswer === updated['answer']) {
                                li.addClass('active');
                            } else {
                                li.removeClass('active');
                            }
                        }
                        $.ajax({
                            url: 'process_edit_question.php',
                            data: data,
                            cache: false,
                            contentType: false,
                            processData: false,
                            method: 'POST',
                            success: function (data) {
                                const json = JSON.parse(data);
                                const updated = json['updated'];
                                if (updated) {

                                    let contentAndImageHtml = updated['content'];
                                    if (updated['image']) {
                                        contentAndImageHtml += `
                                            <br>
                                            <img src="${updated['image']}?${new Date().getTime()}"
                                                style="width: 200px;  display: inline-block;" alt="">
                                        `;
                                    }


                                    $(`tr#${id}`).find('td').each(function (index) {
                                        let td = $(this);

                                        if (index === 1) {
                                            td.empty().html(contentAndImageHtml);
                                        } else if (index === 2) {
                                            td.find('li').each(function (indexLi) {
                                                let li = $(this);
                                                updateListAnswer(['a', 'b', 'c', 'd'][indexLi], li, updated);
                                            })
                                        }
                                    });

                                }

                                const $snackbar = $("#snackbar");
                                $snackbar
                                    .text(json['message'])
                                    .addClass('show');
                                setTimeout(() => $snackbar.removeClass('show'), 2000);

                                editModal.modal('hide');
                            },
                            error: $xhr => {
                                const $snackbar = $("#snackbar");
                                $snackbar
                                    .text(JSON.parse($xhr.responseText)['message'])
                                    .addClass('show');
                                setTimeout(() => $snackbar.removeClass('show'), 2000);

                                editModal.modal('hide');
                            }
                        });

                    });
                },
                error: $xhr => {
                    const $snackbar = $("#snackbar");
                    $snackbar
                        .text(JSON.parse($xhr.responseText)['message'])
                        .addClass('show');
                    setTimeout(() => $snackbar.removeClass('show'), 2000);
                }
            });
        });


        /**
         * Delete question
         */

        $('#table-body').on('click', '.delete-question', function () {
            const id = $(this).prop('id');
            console.log(id);
            const $snackbar = $("#snackbar");

            if (confirm('Bạn có chắc chắn muốn xóa câu hỏi này ? ')) {
                $.ajax({
                    type: 'POST',
                    url: 'process_delete_question.php',
                    data: {id},
                    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                    dataType: 'json',
                    success: data => {
                        const thisRow = $(`tr#${id}`);
                        let thisIndex = thisRow.find("td:first").html();
                        thisRow.nextAll('tr').each(function () {
                            $(this).find("td:first").html(thisIndex++)
                        });
                        thisRow.animate({
                            opacity: 'hide',
                            right: '-500px'
                        }, 'slow', 'linear', function () {
                            $(this).remove();
                            refreshAllPages();
                        });

                        $snackbar
                            .text(data['message'])
                            .addClass('show');
                        setTimeout(() => $snackbar.removeClass('show'), 2000);
                    },
                    error: $xhr => {
                        $snackbar
                            .text(JSON.parse($xhr.responseText)['message'])
                            .addClass('show');
                        setTimeout(() => $snackbar.removeClass('show'), 2000);
                    }
                });
            }
        });


        /**
         * Add question
         */

        let addInputUploadImage = $('#add-input-upload-mage');
        const addImage = $('#add-question-image');

        $('#add-btn-delete-image').click(() => {
            addImage.fadeOut('fast');
            addInputUploadImage.val(null);
        });

        addInputUploadImage.change(e => {
            if (e.target.files && e.target.files[0]) {
                const reader = new FileReader();
                reader.onload = ev => addImage.fadeIn('fast').attr('src', ev.target.result);
                reader.readAsDataURL(e.target.files[0]);
            }
        });

        $('#add-modal').on('show.bs.modal', function () {
            const addModal = $(this);

            const inputContent = $('#add-input-content');
            const answerA = $('#add-input-answer-a');
            const answerB = $('#add-input-answer-b');
            const answerC = $('#add-input-answer-c');
            const answerD = $('#add-input-answer-d');

            inputContent.val('');
            addImage.fadeOut('fast');
            addInputUploadImage.val(null);
            answerA.val('');
            answerB.val('');
            answerC.val('');
            answerD.val('');
            addModal.find('input:radio[name=answer]').each(function () {
                $(this).prop('checked', false);
            });

            $('#modal-button-add-question').off('click').on('click', function () {
                const data = new FormData($('#add-form')[0]);
                $.ajax({
                    url: 'process_add_question.php',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    method: 'POST',
                    success: updateWhenAddedSuccessfully,
                    error: $xhr => {
                        let snackBar = $('#snackbar');
                        snackBar
                            .text(JSON.parse($xhr.responseText)['message'])
                            .addClass('show');
                        setTimeout(() => snackBar.removeClass('show'), 2000);
                    }
                });
            });

            function updateWhenAddedSuccessfully(data) {
                let json = JSON.parse(data);
                let snackBar = $('#snackbar');

                refreshAllPages();

                snackBar
                    .text(json['message'])
                    .addClass('show');
                setTimeout(() => snackBar.removeClass('show'), 2000);
                addModal.modal('hide');
            }
        });
    });

</script>
