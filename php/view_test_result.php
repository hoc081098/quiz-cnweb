<?php
/**
 * Created by PhpStorm.
 * User: Peter Hoc
 * Date: 12/17/2018
 * Time: 10:32 PM
 */

include_once 'check_logged_in_and_role.php';
include_once 'config/connect_db.php';

if (!is_user_logged_in()) {
    header('Location: index.php?open_login=true');
    exit(0);
}


if (isset($_GET['status'])) {
    if ($_GET['status'] === 'error') {
        if (!isset($_GET['score']) || !isset($_GET['name']) || !isset($_GET['time_start']) || !isset($_GET['elapsed_time'])) {
            header('Location: index.php');
            exit(0);
        } else {
            $result = $_GET;
        }
    }
} else {
    if (!isset($_GET['id'])) {
        header('Location: index.php');
        exit(0);
    } else {
        $id = $_GET['id'];
        $stmt = $pdo->prepare('SELECT * FROM details WHERE id = ? LIMIT 1');
        if ($stmt->execute([$id]) && $result = $stmt->fetch()) {
            //continue
        } else {
            header('Location: index.php');
            exit(0);
        }
    }
}


date_default_timezone_set('Asia/Ho_Chi_Minh');


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Result</title>

    <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css">

    <script src="../js/jquery-3.3.1.js" crossorigin="anonymous">
    </script>

    <script src="../js/bootstrap.min.js"></script>

    <style type="text/css">
        body {
            padding: 80px 0px;
        }
    </style>


</head>
<body>

<?php include 'navbar.php'; ?>

<div class="container">
    <div class="card w-75 text-center justify-content-center m-auto mb-4 border-info">
        <div class="card-body">
            <h3 class="card-title text-primary"><a
                        href="detail.php?username=<?php echo $result['name']; ?>"><?php echo $result['name']; ?></a>
            </h3>
            <h4 class="card-subtitle mb-3"> Điểm của bạn là: <span
                        class="text-info font-weight-bold"><?php echo $result['score']; ?></span></h4>
            <div class="card-text">
                <?php if (isset($_GET['status']) && $_GET['status'] === 'error') : ?>
                    <div class="row justify-content-center text-center p-0 m-0">
                        <span class="alert alert-danger">Lưu kết quả bị lỗi</span>
                    </div>
                <?php elseif (isset($result)) : ?>
                    <div class="row justify-content-center text-center p-0 m-0">
                        <a href="detail.php?username=<?php echo $result['name']; ?>&id=<?php echo $result['id']; ?>"
                           class="alert alert-success">Đã
                            lưu kết quả thành công</a>
                    </div>
                <?php endif; ?>
            </div>


            <div class="card-footer bg-white">
                <p class="card-text text-muted small">Thời gian bắt
                    đầu: <?php echo date('H:i:s d/m/Y', $result['time_start']); ?></p>
                <span class="card-text text-muted small">Thời gian làm bài: <?php echo $result['elapsed_time']; ?>
                    s</span>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>

<script>
    $(document).ready(function () {
        $('ul#ul-nav').find('.active').each(function () {
            $(this).removeClass('active');
        });
    });
</script>

</body>
</html>

