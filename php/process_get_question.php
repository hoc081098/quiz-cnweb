<?php


include_once 'config/connect_db.php';
include_once 'const.php';
include_once 'check_logged_in_and_role.php';

if (!is_admin()) {
    header('HTTP/1.0 403 Forbidden');
    echo json_encode(
        ['message' => 'Bạn phải là admin'], JSON_UNESCAPED_UNICODE
    );
    exit();
}

$page = isset($_GET['page']) ? $_GET['page'] : 1;
$page = max($page, 1);

$limit = NUMBER_QUESTION_PER_PAGE;
$start = $limit * ($page - 1);

if (isset($_GET['load_all_page']) && $_GET['load_all_page'] === 'true') {
    $start = 0;
    $limit = NUMBER_QUESTION_PER_PAGE * $page;
}

if (!empty($_GET['search-key'])) {
    $statement = $pdo->prepare(
        'SELECT id, content, a, b, c, d, answer, image,  UNIX_TIMESTAMP(created_at) as created_at,  UNIX_TIMESTAMP(updated_at) as updated_at
          FROM questions
          WHERE content LIKE ? OR
              a LIKE ? OR
              b LIKE ? OR
              c LIKE ? OR
              d LIKE ?
          ORDER BY created_at DESC
          LIMIT ?, ?'
    );
    $search_key = "%{$_GET['search-key']}%";
    if ($statement->execute([$search_key, $search_key, $search_key, $search_key, $search_key, $start, $limit + 1])) {
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    } else {

        header('HTTP/1.0 500 Internal Server Error');
        echo json_encode(
            [
                'message' => 'Lỗi khi truy vấn'],
            JSON_UNESCAPED_UNICODE
        );

    }

} else {
    $statement = $pdo->prepare(
        'SELECT id, content, a, b, c, d, answer, image,  UNIX_TIMESTAMP(created_at) as created_at,  UNIX_TIMESTAMP(updated_at) as updated_at
          FROM questions
          ORDER BY created_at DESC
          LIMIT ?, ?'
    );
    if ($statement->execute([$start, $limit + 1])) {
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
    } else {

        header('HTTP/1.0 500 Internal Server Error');
        echo json_encode(
            [
                'message' => 'Lỗi khi truy vấn'],
            JSON_UNESCAPED_UNICODE
        );

    }
}

function is_active($cur_answer, $answer): string
{
    return $answer == $cur_answer ? ' active ' : '';
}

date_default_timezone_set('Asia/Ho_Chi_Minh');
?>

<?php foreach (array_slice($results, 0, $limit) as $index => $res):
    ?>

    <tr id="<?php echo $res['id']; ?>">
        <td scope="row"
            class="font-weight-bold"><?php echo str_pad(
                $index + 1 + $start, 2, '0', STR_PAD_LEFT
            ); ?></td>
        <td>
            <?php echo $res['content']; ?>
            <?php if (isset($res['image'])) : ?>
                <br>
                <img src="<?php echo $res['image'] . '?' . Date('U') ?>"
                     style="width: 200px;  display: inline-block;"
                     class=".img-thumbnail rounded"
                     alt="">
            <?php endif; ?>
        </td>

        <td>
            <ul class="list-group list-group-flush">
                <li class="list-group-item <?php echo is_active(
                    'a', $res['answer']
                ); ?>"><?php echo 'A. ' . $res['a'] ?></li>
                <li class="list-group-item <?php echo is_active(
                    'b', $res['answer']
                ); ?>"><?php echo 'B. ' . $res['b'] ?></li>
                <li class="list-group-item <?php echo is_active(
                    'c', $res['answer']
                ); ?>"><?php echo 'C. ' . $res['c'] ?></li>
                <li class="list-group-item <?php echo is_active(
                    'd', $res['answer']
                ); ?>"><?php echo 'D. ' . $res['d'] ?></li>
            </ul>
        </td>

        <td>
            <button class="btn btn-default" data-toggle="modal"
                    data-target="#edit-modal"
                    id="<?php echo $res['id']; ?>">
                <i class="material-icons"
                   style="vertical-align: middle; color: #536dfe ;">edit</i>
            </button>
        </td>
        <td>
            <button class="btn btn-danger delete-question" type="button"
                    value="" id="<?php echo $res['id']; ?>">
                <i class="material-icons" style="vertical-align: middle; ">delete</i>
            </button>
        </td>
    </tr>

<?php endforeach; ?>

<input type="hidden" name="load-all-data"
       value="<?php echo count($results) <= $limit; ?>">