<?php
include_once 'check_logged_in_and_role.php';
?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">Quiz CN Web</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto" id="ul-nav">
                <li class="nav-item active" id="li-nav-home">
                    <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                </li>

                <?php if (is_user_logged_in()) : ?>
                    <li class="nav-item" id="li-nav-test">
                        <a class="nav-link" href="test.php">Thi ngay</a>
                    </li>

                    <li class="nav-item" id="li-nav-detail">
                        <a class="nav-link"
                           href="detail.php?username=<?php echo $_SESSION[USER_NAME]; ?>">Kết
                            quả</a>
                    </li>
                <?php endif; ?>

                <?php if (is_admin()) : ?>
                    <li class="nav-item dropdown" id="li-nav-manage">
                        <a class="nav-link dropdown-toggle" href="#"
                           id="navbarDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false">
                            Quản lý
                        </a>
                        <div class="dropdown-menu"
                             aria-labelledby="navbarDropdown">
                            <a class="dropdown-item active" id="li-nav-manage-questions"
                               href="manage_questions.php">Câu hỏi</a>
                            <a class="dropdown-item" href="manage_users.php" id="li-nav-manage-users">User</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="search_questions.php" id="li-nav-search-questions"><i
                                        class="material-icons" style="vertical-align: middle;">search</i>Tìm kiếm câu
                                hỏi</a>
                        </div>
                    </li>
                <?php endif; ?>

            </ul>
            <form class="form-inline my-2 my-lg-0 mr-4" method="get"
                  action="index.php">
                <input class="form-control mr-sm-2" type="search" name="search-key"
                       placeholder="Tìm kiếm user..."
                       aria-label="Search">
                <button class="btn btn-success my-2 my-sm-0" type="submit">
                    Search
                </button>
            </form>

            <?php if (is_user_logged_in()) : ?>
                <ul class="navbar-nav">
                    <li class="nav-item dropdown pull-right">
                        <a href="#" class="nav-link dropdown-toggle"
                           id="navWelcomeUser" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            Xin chào <?php echo $_SESSION[FULL_NAME] ?>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right"
                             aria-labelledby="navWelcomeUser">
                            <a class="dropdown-item"
                               href="edit_user.php?name=<?php echo $_SESSION[USER_NAME]; ?>">Chỉnh
                                sửa thông tin</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#" id="logout">Logout</a>
                        </div>
                    </li>
                </ul>
            <?php else: ?>
                <button type="button" class="btn btn-primary"
                        data-toggle="modal" data-target="#loginModal">
                    Login
                </button>
            <?php endif; ?>
        </div>
    </div>
</nav>
<?php include 'login_modal.php'; ?>
<?php include 'register_modal.php' ?>
