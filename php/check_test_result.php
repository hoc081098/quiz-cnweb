<?php
/**
 * Created by PhpStorm.
 * User: Peter Hoc
 * Date: 12/12/2018
 * Time: 3:29 PM
 */

$endTime = new DateTime();


include_once 'config/connect_db.php';
include_once 'check_logged_in_and_role.php';

if (!is_user_logged_in()) {
    header('Location: index.php?open_login=true');
    exit();
}

if (isset($_POST['start-time'])) {
    $startTime = $_POST['start-time'];

    $numberRightAnswer = array_sum(
        array_map(
            function ($answer, $question_id) use ($pdo) {
                $statement = $pdo->prepare(
                    'SELECT COUNT(*) AS rows FROM questions WHERE id = ? AND answer = ? LIMIT 1'
                );
                if ($statement->execute([$question_id, $answer])) {
                    $row = $statement->fetch();
                    return $row ? $row['rows'] : 0;
                }
                return 0;
            },
            $_POST,
            array_keys($_POST)
        )
    );
    $score = $numberRightAnswer * 10;

    $statement = $pdo->prepare(
        'INSERT INTO details(name ,time_start, elapsed_time, score) VALUES(?, ?, ?, ?)'
    );
    $username = $_SESSION[USER_NAME];
    $elapsedTime = $endTime->getTimestamp() - $startTime;

    if ($statement->execute([
        $username,
        $startTime,
        $elapsedTime,
        $score
    ])) {
        header("Location: view_test_result.php?id={$pdo->lastInsertId()}");
        exit(0);
    } else {
        header("Location: view_test_result.php?status=error&score={$score}&name={$username}&time_start={$startTime}&elapsed_time={$elapsedTime}");
        exit(0);
    }
} else {
    header('Location: ' . ($_SERVER['HTTP_REFERER'] ?: 'index.php'));
    exit(0);
}