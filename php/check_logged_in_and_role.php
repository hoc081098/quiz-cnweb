<?php
/**
 * Created by PhpStorm.
 * User: Peter Hoc
 * Date: 12/11/2018
 * Time: 3:03 PM
 */


define('USER_TYPE', 'user_type');
define('USER_NAME', 'user_name');
define('FULL_NAME', 'full_name');
define('ADMIN', 'admin');
define('NORMAL_USER', 'normal_user');

session_start();

/**
 * @return bool
 */
function is_user_logged_in()
{
    return isset($_SESSION[USER_TYPE]);
}

/**
 * @return bool
 */
function is_admin()
{
    return is_user_logged_in() && $_SESSION[USER_TYPE] == ADMIN;
}

/**
 * @return bool
 */
function is_normal_user()
{
    return is_user_logged_in() && $_SESSION[USER_TYPE] == NORMAL_USER;
}

