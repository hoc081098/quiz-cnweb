<?php

include_once 'config/connect_db.php';
include_once 'const.php';

$title = 'Điểm cao';
$searchCondition = '';

if (isset($_GET['search-key'])) {
    $searchKey = $_GET['search-key'];
    $title = "Kết quả tìm kiếm của '{$searchKey}'";
    $searchCondition = "
        WHERE ud.name LIKE '%{$searchKey}%'
        OR ud.full_name LIKE '%{$searchKey}%'
        OR ud.class LIKE '%{$searchKey}%'
        OR ud.birthday LIKE '%{$searchKey}%'
    ";
}

$results = $pdo->query(
    "SELECT ud.name, ud.full_name, ud.class, DATE_FORMAT(ud.birthday, '%d/%m/%Y') as birthday, SUM(score) as sum_score, SUM(elapsed_time) as sum_elapsed_time, COUNT(id) as count_test
         FROM userdetails ud LEFT JOIN details d on ud.name = d.name
         {$searchCondition}
         GROUP BY ud.name
         ORDER BY sum_score DESC, elapsed_time ASC, count_test ASC, name
         LIMIT " . LIMIT_HIGH_SCORE
);

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Trang chủ</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link href="../css/bootstrap.min.css" rel="stylesheet"
          type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">


    <script src="../js/jquery-3.3.1.js" crossorigin="anonymous">
    </script>

    <script src="../js/bootstrap.min.js"
            crossorigin="anonymous"></script>


    <style type="text/css">
        body {
            padding: 80px 0;
        }

        .table > tbody > tr > td {
            vertical-align: middle;
        }
    </style>
</head>

<body>


<?php include 'navbar.php'; ?>

<!-- Page Content -->
<div class="container">
    <h3 class="text-center mb-4"><?php echo $title; ?></h3>

    <?php

    if (isset($_SESSION['removed-user']) && $_SESSION['removed-user'] === 'error') {
        echo '<div class="row justify-content-center text-center p-0 m-0">
                    <span class="alert alert-warning removed-user-msg">Xóa user không thành công</span>
               </div>';
        unset($_SESSION['removed-user']);
    } elseif (isset($_SESSION['removed-user']) && $_SESSION['removed-user'] === 'success') {
        echo '<div class="row justify-content-center text-center p-0 m-0">
                    <span class="alert alert-success removed-user-msg">Xóa users thành công</span>
              </div>';
        unset($_SESSION['removed-user']);
    }
    ?>


    <table class="table table-bordered text-center table-hover table-responsive-md">
        <thead>
        <tr class="bg-info text-white">
            <th scope="col">STT</th>
            <th scope="col">Username</th>
            <th scope="col">Họ tên</th>
            <th scope="col">Lớp</th>
            <th scope="col">Ngày sinh(dd/MM/yyyy)</th>
            <th scope="col">Tổng điểm</th>
            <th scope="col">Tổng thời gian(s)</th>
            <th scope="col">Số lần thi</th>
            <?php if (is_admin()): ?>
                <th scope="col">Chỉnh sửa</th>
                <th scope="col">Xóa</th>
            <?php endif; ?>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($results as $index => $result) : ?>
            <tr>
                <td scope="row"
                    class="font-weight-bold"><?php echo str_pad(
                        $index + 1, 2, '0', STR_PAD_LEFT
                    ); ?></td>
                <td class="font-weight-bold"><a
                            href="detail.php?username=<?php echo $result['name']; ?>"><?php echo $result['name']; ?></a>
                </td>
                <td><?php echo $result['full_name']; ?></td>
                <td><?php echo $result['class']; ?></td>
                <td><?php echo $result['birthday']; ?></td>
                <td><?php echo $result['sum_score'] ?: 0; ?></td>
                <td><?php echo $result['sum_elapsed_time'] ?: 0; ?></td>
                <td><?php echo $result['count_test'] ?: 0; ?></td>
                <?php if (is_admin()): ?>
                    <td>
                        <a class="btn btn-default "
                           href="edit_user.php?name=<?php echo $result['name']; ?>">
                            <i class="material-icons"
                               style="vertical-align: middle;">edit</i>
                        </a>
                    </td>
                    <td>
                        <form action="process_delete_user.php" method="post"
                              class="form-inline">
                            <button class="btn btn-danger delete-user"
                                    type="submit" value="">
                                <i class="material-icons"
                                   style="vertical-align: middle;">delete</i>
                            </button>
                            <input type="hidden" name="name"
                                   value="<?php echo $result['name']; ?>">
                        </form>
                    </td>
                <?php endif; ?>
            </tr>
        <?php endforeach; ?>
        </tbody>

    </table>
</div>
<!-- /.container -->

<?php include 'footer.php'; ?>


<script>
    $(() => {
        $('ul#ul-nav').find('.active').each(function () {
            $(this).removeClass('active');
        });
        $('#li-nav-home').addClass('active');

        $('.delete-user').click(e => {
            let c;
            if (c = confirm('Bạn có chắc chắn muốn xóa user này không?')) {
                $('#delete-user-form').submit();
            }
            return c;
        });

        let openLogin = "<?php echo isset($_GET['open_login']) ? $_GET['open_login'] : false ?>";
        if (openLogin === 'true') {
            $('#loginModal').modal('show');
        }


        $('.removed-user-msg').fadeOut(2000, function () {
            $(this).remove();
        });

    });
</script>

</body>
</html>
