<?php
/**
 * Created by PhpStorm.
 * User: Peter Hoc
 * Date: 12/11/2018
 * Time: 6:44 PM
 */

include_once 'config/connect_db.php';;
if (!isset($_GET['username'])) {
    header('Location: ' . ($_SERVER['HTTP_REFERER'] ?: 'index.php'));
    exit();
}

$username = $_GET['username'];
$detailStmt = $pdo->prepare(
    'SELECT u.name, full_name, class, birthday, UNIX_TIMESTAMP(created_at) as created_at,UNIX_TIMESTAMP(updated_at) as updated_at
              FROM userdetails INNER JOIN users u on userdetails.name = u.name
              WHERE u.name = ? LIMIT 1'
);
if ($detailStmt->execute([$username])) {
    $detail = $detailStmt->fetch();
    if (!$detail) {
        $errorMsg = 'Không tồn tại user với username là ' . $username;
    }
} else {
    header('Location: ' . ($_SERVER['HTTP_REFERER'] ?: 'index.php'));
    exit();
}


$testDetailStmt = $pdo->prepare(
    'SELECT id, score, elapsed_time,  time_start FROM details WHERE name = ? ORDER BY time_start DESC'
);
if ($testDetailStmt->execute([$username])) {
    $details = $testDetailStmt->fetchAll(PDO::FETCH_ASSOC);
} else {
    header('Location: ' . ($_SERVER['HTTP_REFERER'] ?: 'index.php'));
    exit();
}

date_default_timezone_set('Asia/Ho_Chi_Minh');

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Detail</title>

    <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../css/toast.css" type="text/css">

    <script src="../js/jquery-3.3.1.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <style type="text/css">
        body {
            padding: 80px 0;
        }

        .table > tbody > tr > td {
            vertical-align: middle;
            text-align: center;
        }
    </style>
</head>
<body>


<?php include "navbar.php"; ?>

<div class="container">
    <div class="mx-5 px-5">
        <h3 class="text-center mb-4">Kết quả các lần thi</h3>

        <?php if (isset($errorMsg) && !empty($errorMsg)) : ?>

            <div class="text-center  alert-warning p-4"><?php echo $errorMsg; ?></div>

        <?php else : ?>

            <div class="card w-75 text-center justify-content-center mx-auto mb-4 border-info">
                <div class="card-body">
                    <h4 class="card-title text-primary"><a
                                href="detail.php?username=<?php echo $detail['name']; ?>"><?php echo $detail['full_name']; ?></a>
                    </h4>
                    <div class="card-text">
                        <p>
                            <span class="font-weight-bold">Username: </span><?php echo $detail['name']; ?>
                        </p>
                        <p>
                            <span>Lớp: </span><?php echo $detail['class']; ?> |
                            <span>Ngày sinh: </span><?php echo $detail['birthday']; ?>
                        </p>
                    </div>


                    <div class="card-footer bg-white">
                        <p class="card-text text-muted small">Ngày tham
                            gia: <?php echo date('H:i:s d/m/Y', $detail['created_at']); ?></p>
                        <span class="card-text text-muted small">Lần cập nhật cuối: <?php echo date('H:i:s d/m/Y', $detail['updated_at']); ?></span>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center mb-3">
                <?php if (is_admin()): ?>
                    <a class="btn btn-secondary mx-4"
                       href="edit_user.php?name=<?php echo $detail['name']; ?>">
                        <i class="material-icons"
                           style="vertical-align: middle;">edit</i>
                    </a>
                    <form action="process_delete_user.php" method="post"
                          class="form-inline mx-4">
                        <button class="btn btn-danger delete-user"
                                type="submit" value="">
                            <i class="material-icons"
                               style="vertical-align: middle;">delete</i>
                        </button>
                        <input type="hidden" name="name"
                               value="<?php echo $detail['name']; ?>">
                    </form>
                <?php endif; ?>
            </div>

            <table class="table table-bordered text-center table-hover">
                <thead>
                <tr class="bg-secondary text-white">
                    <th scope="col">STT</th>
                    <th scope="col">Điểm</th>
                    <th scope="col">Thời gian làm bài (s)</th>
                    <th scope="col">Bắt đầu</th>
                    <?php if (is_admin()): ?>
                        <th scope="col">Xóa</th>
                    <?php endif; ?>
                </tr>
                </thead>

                <tbody>
                <?php foreach ($details as $index => $result) : ?>
                    <tr id="<?php echo $result['id']; ?>">
                        <td scope="row"
                            class="font-weight-bold"><?php echo str_pad(
                                $index + 1, 2, '0', STR_PAD_LEFT
                            ); ?></td>
                        <td><?php echo $result['score']; ?></td>
                        <td><?php echo $result['elapsed_time']; ?></td>
                        <td><?php echo date(
                                'H:i:s d/m/Y', $result['time_start']
                            ); ?></td>
                        <?php if (is_admin()): ?>
                            <td>
                                <form action="process_delete_detail_result.php"
                                      class="delete-detail-form" method="post"
                                      id="<?php echo $result['id']; ?>">
                                    <button class="btn btn-danger delete-detail"
                                            type="submit"
                                            value="<?php echo $result['id']; ?>">
                                        <i class="material-icons"
                                           style="vertical-align: middle; ">delete</i>
                                    </button>
                                    <input type="hidden" name="id"
                                           value="<?php echo $result['id']; ?>">
                                </form>
                            </td>
                        <?php endif; ?>
                    </tr>
                <?php endforeach; ?>
                </tbody>

            </table>

        <?php endif; ?>

    </div>

</div>

<?php include "footer.php"; ?>

<?php

if (isset($_SESSION['removed-detail']) && $_SESSION['removed-detail'] === 'error') {
    echo '<div id="snackbar" class="show">Xóa không thành công</div>';
    unset($_SESSION['removed-detail']);
} elseif (isset($_SESSION['removed-detail']) && $_SESSION['removed-detail'] === 'success') {
    echo '<div id="snackbar" class="show">Xóa thành công</div>';
    unset($_SESSION['removed-detail']);
}
?>

<script>
    $(function () {
        $('ul#ul-nav').find('.active').each(function () {
            $(this).removeClass('active');
        });
        $('#li-nav-detail').addClass('active');

        $('.delete-detail').click(function (e) {
            if (!confirm('Bạn có chắc chắn muốn xóa kết quả này không?')) {
                e.preventDefault();
            }
        });

        $('.delete-user').click(e => {
            let c;
            if (c = confirm('Bạn có chắc chắn muốn xóa user này không?')) {
                $('#delete-user-form').submit();
            }
            return c;
        });

        const id = "<?php echo isset($_GET['id']) ? $_GET['id'] : '' ?>";
        if (id.length > 0) {
            const row = $(`tr#${id}`);
            if (row) {
                row.css('background', '#baccd7');
            }
        }

        console.log('Start...');
        setTimeout(() => {
            $('#snackbar').removeClass('show');
            console.log('Remove...');
        }, 2000);
    });
</script>


</body>
</html>
