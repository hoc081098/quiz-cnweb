<?php
/**
 * Created by PhpStorm.
 * User: Peter Hoc
 * Date: 12/12/2018
 * Time: 9:26 PM
 */

include_once 'check_logged_in_and_role.php';
include_once 'config/connect_db.php';

$postUserName = $_POST['name'];
$fullName = $_POST['full-name'];
$class = $_POST['class'];
$birthday = $_POST['birthday'];

if (isset($postUserName) && isset($fullName) && isset($class)
    && isset($birthday)
) {
    if ((is_user_logged_in() && $postUserName == $_SESSION[USER_NAME])
        || is_admin()
    ) {

    } else {

        header('HTTP/1.0 403 Forbidden');
        echo json_encode(
            ['message' => 'Bạn cần login chính user này, hoặc là admin để chỉnh sửa user'],
            JSON_UNESCAPED_UNICODE
        );
        exit();

    }
} else {

    header('HTTP/1.0 422 Unprocessable Entity');
    echo json_encode(
        ['message' => 'Các trường không được rỗng'], JSON_UNESCAPED_UNICODE
    );
    exit();

}


$statementUpdateDetail = $pdo->prepare(
    "UPDATE userdetails SET full_name = ?, class = ?, birthday = STR_TO_DATE(?, '%Y-%m-%d') WHERE name = ?"
);

$statementUpdateUser = $pdo->prepare('UPDATE users SET updated_at = NOW() WHERE name = ?');

if ($statementUpdateDetail->execute([$fullName, $class, $birthday, $postUserName]) && $statementUpdateUser->execute([$postUserName])) {
    $getQuery = $pdo->prepare(
        'SELECT * FROM userdetails WHERE name = ? LIMIT 1'
    );
    if ($getQuery->execute([$postUserName]) && $updated = $getQuery->fetch()) {

        $isCurrentUser = $_SESSION[USER_NAME] === $postUserName;
        if ($isCurrentUser) {
            $_SESSION[FULL_NAME] = $updated['full_name'];
        }

        header('HTTP/1.0 200 OK');
        echo json_encode(
            [
                'message' => 'Cập nhật thông tin user thành công',
                'updated_user' => $updated,
                'is_current_user' => $isCurrentUser
            ], JSON_UNESCAPED_UNICODE
        );

    } else {
        header('HTTP/1.0 200 OK');
        echo json_encode(
            [
                'message' => 'Cập nhật thông tin user thành công'
            ], JSON_UNESCAPED_UNICODE
        );
    }
} else {
    header('HTTP/1.0 500 Internal Server Error');
    echo json_encode(
        [
            'message' => 'Cập nhật thông tin user thất bại'
        ], JSON_UNESCAPED_UNICODE
    );
}

