<?php
/**
 * Created by PhpStorm.
 * User: Peter Hoc
 * Date: 12/13/2018
 * Time: 9:23 PM
 */


include_once 'check_logged_in_and_role.php';
include_once 'const.php';
include_once 'config/connect_db.php';


if (!is_admin()) {
    header('HTTP/1.0 403 Forbidden');
    echo json_encode(
        ['message' => 'Bạn phải là admin'], JSON_UNESCAPED_UNICODE
    );
    exit();
}

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['id'])) {
    $statement = $pdo->prepare('DELETE FROM questions WHERE id = ?');

    if ($statement->execute([$_POST['id']])) {

        foreach (
            array_map('unlink', glob('../images/' . $_POST['id'] . '.*')) as
            $fileName
        ) {
            if (file_exists($fileName)) {
                unlink($fileName);
            }
        }

        header('HTTP/1.0 200 OK');
        echo json_encode(
            ['message' => 'Xóa câu hỏi thành công'], JSON_UNESCAPED_UNICODE
        );
        exit();

    } else {

        header('HTTP/1.0 500 Internal Server Error');
        echo json_encode(
            ['message' => 'Lỗi khi xóa câu hỏi'], JSON_UNESCAPED_UNICODE
        );
        exit();

    }

} else {
    header('HTTP/1.0 405 Method Not Allowed');
    echo json_encode(['message' => 'Yêu cầu id'], JSON_UNESCAPED_UNICODE);
    exit();
}