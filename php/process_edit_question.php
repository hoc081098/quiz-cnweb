<?php
/**
 * Created by PhpStorm.
 * User: Peter Hoc
 * Date: 12/13/2018
 * Time: 9:23 PM
 */


include_once 'check_logged_in_and_role.php';
include_once 'const.php';
include_once 'config/connect_db.php';

if (!is_admin()) {
    header('HTTP/1.0 403 Forbidden');
    echo json_encode(
        ['message' => 'Bạn phải là admin'], JSON_UNESCAPED_UNICODE
    );
    exit();
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if (!isset($_POST['id']) || !isset($_POST['content']) || !isset($_POST['a'])
        || !isset($_POST['b'])
        || !isset($_POST['c'])
        || !isset($_POST['d'])
        || !isset($_POST['answer'])
    ) {

        header('HTTP/1.0 405 Method Not Allowed');
        echo json_encode(
            ['message' => 'Các trường không được rỗng'], JSON_UNESCAPED_UNICODE
        );
        exit();

    }

    if (isset($_FILES['input-question-image'])
        && $_FILES['input-question-image']['error'] == 0
    ) {
        $ext = pathinfo(
            $_FILES['input-question-image']['name'], PATHINFO_EXTENSION
        );

        if (isset($ext)
            && !in_array(
                strtolower($ext), ['jpeg', 'jpg', 'png', 'gif', 'bmp']
            )
        ) {
            header('HTTP/1.0 405 Method Not Allowed');
            echo json_encode(
                ['message' => 'Định dạng ảnh cho phép: ' . join(
                        ['jpeg', 'jpg', 'png', 'gif', 'bmp'], ', '
                    )], JSON_UNESCAPED_UNICODE
            );
            exit();
        }

        $newName = $_POST['id'] . '.' . $ext;
        $path = '../images/' . $newName;

        foreach (
            array_map('unlink', glob('../images/' . $_POST['id'] . '.*')) as
            $fileName
        ) {
            if (file_exists($fileName)) {
                unlink($fileName);
            }
        }
        if (file_exists($path)) {
            unlink($path);
        }

        if (!move_uploaded_file(
            $_FILES['input-question-image']['tmp_name'], $path
        )
        ) {
            header('HTTP/1.0 500 Internal Server Error');
            echo json_encode(
                ['message' => 'Lỗi khi lưu file'], JSON_UNESCAPED_UNICODE
            );
            exit();
        }

        $imagePath = $path;
    } else {
        $imagePath = isset($_POST['original-image']) ? $_POST['original-image']
            : NULL;
        if ($imagePath == NULL) {
            // delete all image
            foreach (
                array_map('unlink', glob('../images/' . $_POST['id'] . '*.*')) as
                $fileName
            ) {
                if (file_exists($fileName)) {
                    unlink($fileName);
                }
            }
        }
    }

    $stmtUpdate = $pdo->prepare(
        'UPDATE questions SET content = ?, a = ?, b = ?, c = ?, d = ?, answer = ?, image = ? WHERE id = ?'
    );
    if ($stmtUpdate->execute(
        [
            $_POST['content'],
            $_POST['a'],
            $_POST['b'],
            $_POST['c'],
            $_POST['d'],
            $_POST['answer'],
            $imagePath,
            $_POST['id']
        ]
    )
    ) {

        $statement = $pdo->prepare(
            'SELECT * FROM questions WHERE id = ? LIMIT 1'
        );
        if ($statement->execute([$_POST['id']])
            && $question = $statement->fetch()
        ) {

            header('HTTP/1.0 200 OK');
            echo json_encode(
                [
                    'message' => 'Update câu hỏi thành công',
                    'updated' => $question
                ], JSON_UNESCAPED_UNICODE
            );
            exit();

        } else {

            header('HTTP/1.0 500 Internal Server Error');
            echo json_encode(
                [
                    'message' => 'Update câu hỏi thành công'],
                JSON_UNESCAPED_UNICODE
            );
            exit();

        }
    } else {
        header('HTTP/1.0 500 Internal Server Error');
        echo json_encode(
            ['message' => 'Lỗi khi update câu hỏi'], JSON_UNESCAPED_UNICODE
        );
        exit();
    }

} else {
    if ($_SERVER['REQUEST_METHOD'] == 'GET') {

        if (isset($_GET['question-id'])) {
            $questionId = $_GET['question-id'];
            $statement = $pdo->prepare(
                'SELECT * FROM questions WHERE id = ? LIMIT 1'
            );
            if ($statement->execute([$questionId])
                && $question = $statement->fetch()
            ) {

                header('HTTP/1.0 200 OK');
                echo json_encode(
                    [
                        'message' => 'Lấy thông tin câu hỏi thành công',
                        'question' => $question
                    ], JSON_UNESCAPED_UNICODE
                );

            } else {

                header('HTTP/1.0 500 Internal Server Error');
                echo json_encode(
                    [
                        'message' => 'Không tìm thấy câu hỏi với id = '
                            . $questionId . ', hoặc lỗi khi truy vấn'],
                    JSON_UNESCAPED_UNICODE
                );

            }

        } else {

            header('HTTP/1.0 422 Unprocessable Entity');
            echo json_encode(
                ['message' => 'Các trường không được rỗng'],
                JSON_UNESCAPED_UNICODE
            );
            exit();

        }
    }
}
