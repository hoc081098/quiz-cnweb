<?php
/**
 * Created by PhpStorm.
 * User: Peter Hoc
 * Date: 12/12/2018
 * Time: 3:05 PM
 */

define('ROOT_PATH', __DIR__);
define('MAX_NUMBER_QUESTION', 10);
define('LIMIT_HIGH_SCORE', 10);
define('NUMBER_QUESTION_PER_PAGE', 10);