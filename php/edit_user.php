<?php
/**
 * Created by PhpStorm.
 * User: Peter Hoc
 * Date: 12/12/2018
 * Time: 8:31 PM
 */

include_once 'check_logged_in_and_role.php';
include_once 'config/connect_db.php';

$postUserName = $_GET['name'];

if (isset($postUserName)) {
    if (is_user_logged_in() && $postUserName == $_SESSION[USER_NAME]) {
        //continue
    } else {
        if (is_admin()) {
            //continue
        } else {
            $msg
                = 'Bạn cần login chính user này, hoặc là admin để chỉnh sửa user';
        }
    }
} else {
    header('Location: ' . ($_SERVER['HTTP_REFERER'] ?: 'index.php'));
    exit();
}


$statement = $pdo->prepare("SELECT name, full_name, class, DATE_FORMAT(birthday, '%Y-%m-%d') as birthday FROM userdetails WHERE name = ? LIMIT 1");
if ($statement->execute([$postUserName])) {
    $row = $statement->fetch(PDO::FETCH_ASSOC);
    if (!$row) {
        $msg = 'User không tồn tại';
    }

} else {
    $msg = 'Có lỗi xảy ra khi lấy thông tin user';
}


?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Edit user</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="../css/bootstrap.min.css" rel="stylesheet"
          type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="../css/toast.css" type="text/css">


    <script src="../js/jquery-3.3.1.js" crossorigin="anonymous">
    </script>

    <script src="../js/bootstrap.min.js"
            crossorigin="anonymous"></script>



    <style type="text/css">
        body {
            padding: 80px 0;
        }
    </style>
</head>
<body>

<?php include 'navbar.php'; ?>

<div class="container">
    <div class="mx-5 px-5">
        <h3 class="text-center mb-4">Cập nhật thông tin user</h3>

        <?php if (isset($msg) && !empty($msg)) : ?>

            <div class="text-center  alert-warning p-4"><?php echo $msg; ?></div>

        <?php else : ?>

            <form>
                <div class="form-group">
                    <label for="input-username">Username</label>
                    <input readonly
                           type="text"
                           class="form-control"
                           id="input-username"
                           placeholder=""
                           name="name"
                           value="<?php echo $row['name'] ?>">
                </div>

                <div class="form-group">
                    <label for="input-full-name">Họ tên</label>
                    <input type="text"
                           class="form-control"
                           id="input-full-name"
                           placeholder="Họ tên"
                           name="full-name"
                           value="<?php echo $row['full_name'] ?>">
                </div>
                <div class="form-group">
                    <label for="input-class">Lớp</label>
                    <input type="text"
                           class="form-control"
                           id="input-class"
                           placeholder="Lớp"
                           name="class"
                           value="<?php echo $row['class'] ?>">
                </div>

                <div class="form-group">
                    <label for="input-birthday">Ngày sinh</label>
                    <input type="date"
                           class="form-control"
                           id="input-birthday"
                           name="birthday"
                           value="<?php echo $row['birthday'] ?>">
                </div>

                <button type="button" class="btn btn-primary" name="submit"
                        value="submit">Cập nhật
                </button>
            </form>

        <?php endif; ?>
    </div>

    <div id="snackbar">Some text some message..</div>
</div>

<?php include 'footer.php'; ?>


<script>

    $(() => {
        $('[name=submit]').click(e => {
            e.preventDefault();

            $.ajax({
                type: 'POST',
                url: 'process_edit_user.php',
                data: $('form').serialize(),
                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                dataType: 'json',
                success: data => {
                    console.log({data});
                    if (data['is_current_user'] === true) {
                        $('#navWelcomeUser').text(`Xin chào ${data['updated_user']['full_name']}`);
                    }

                    const $snackbar = $("#snackbar");
                    $snackbar
                        .text(data['message'])
                        .addClass('show');
                    setTimeout(() => $snackbar.removeClass('show'), 2000);
                },
                error: $xhr => {
                    const $snackbar = $("#snackbar");
                    $snackbar
                        .text(JSON.parse($xhr.responseText)['message'])
                        .addClass('show');
                    setTimeout(() => $snackbar.removeClass('show'), 2000);
                }
            });
        });
    });

</script>

</body>
</html>
