<?php
/**
 * Created by PhpStorm.
 * User: Peter Hoc
 * Date: 12/11/2018
 * Time: 3:36 PM
 */

?>

<!-- Login Modal -->
<div class="modal fade" id="loginModal" role="dialog"
     aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Login</h5>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control" id="username"
                               name="username" placeholder="Enter username"
                               required autofocus>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input name="password"
                               type="password" class="form-control"
                               id="password" placeholder="Password"
                               required>
                    </div>
                </form>
                <div class="text-center alert" style="display: none;"
                     id="login-msg"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info"
                        data-toggle="modal"
                        data-target="#registerModal"
                        data-dismiss="modal">Đăng kí
                </button>
                <button type="button" class="btn btn-secondary"
                        data-dismiss="modal">Close
                </button>
                <button type="submit" id="login-submit" name="login-submit"
                        class="btn btn-primary">Login
                </button>
            </div>
        </div>
    </div>
</div>
<!-- End Login Modal -->

<script>

    $(document).ready(() => {
        const performLogin = () => {
            const $loginMsg = $('#login-msg');
            const username = $('#username').val();
            const password = $('#password').val();

            let msgValidate = '';
            if (!username || username.length < 3) {
                msgValidate += '<p>Tên quá ngắn, ít nhất 3 kí tự</p>';
            }
            if (!password || password.length < 6) {
                msgValidate += '<p>Mật khẩu quá ngắn quá ngắn, ít nhất 6 kí tự</p>';
            }

            if (msgValidate.length > 0) {
                $loginMsg
                    .removeClass('alert-primary')
                    .removeClass('alert-success')
                    .addClass('alert-warning')
                    .html(msgValidate)
                    .stop(true, true)
                    .show()
                    .fadeOut(2000);
                return;
            }


            $loginMsg
                .addClass('alert-primary')
                .removeClass('alert-success')
                .removeClass('alert-warning')
                .stop(true, true)
                .text('Loading...')
                .show();

            $.ajax({
                    url: 'process_login_logout.php',
                    method: 'POST',
                    data: {
                        action: 'ACTION_LOGIN',
                        username: username,
                        password: password
                    },
                    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                    dataType: 'json',
                    success: data => {

                        $loginMsg
                            .removeClass('alert-primary')
                            .addClass('alert-success')
                            .removeClass('alert-warning')
                            .text(data['message'])
                            .stop(true, true)
                            .show()
                            .fadeOut(1800, () => {
                                $('#loginModal').modal('hide');
                                window.location = window.location.pathname;
                            });
                    },
                    error: $xhr => {
                        $loginMsg
                            .removeClass('alert-primary')
                            .removeClass('alert-success')
                            .addClass('alert-warning')
                            .text(JSON.parse($xhr.responseText)['message'])
                            .stop(true, true)
                            .show()
                            .fadeOut(1800);
                    }
                }
            );
        }

        $('#loginModal').find('input').each(function () {
            console.log($(this));
            $(this).keypress(function (e) {
                if (e.which === 13) {
                    e.preventDefault();
                    performLogin();
                    return false;
                }
            });
        });

        $('#login-submit').click(function (e) {
            e.preventDefault();
            performLogin();
        });
    })
    ;
</script>

<script>
    $(document).ready(() => {
        $('#logout').click(e => {
            e.preventDefault();
            $.ajax({
                url: 'process_login_logout.php',
                method: 'POST',
                data: {
                    action: 'ACTION_LOGOUT'
                },
                success: function (data) {
                    console.log(data);
                    window.location.replace('index.php');
                }
            });
        });
    });

</script>