<?php
/**
 * Created by PhpStorm.
 * User: Peter Hoc
 * Date: 12/13/2018
 * Time: 1:13 AM
 */

include_once 'check_logged_in_and_role.php';
include_once 'config/connect_db.php';

if (isset($_POST['id']) && is_admin()) {
    if ($pdo->prepare('DELETE FROM details WHERE id = ?')->execute([$_POST['id']])) {
        $_SESSION['removed-detail'] = 'success';
    } else {
        $_SESSION['removed-detail'] = 'error';
    }
}

header('Location: ' . ($_SERVER['HTTP_REFERER'] ?: 'index.php'));
exit();