<?php
/**
 * Created by PhpStorm.
 * User: Peter Hoc
 * Date: 11/18/2018
 * Time: 8:01 PM
 */
include_once 'check_logged_in_and_role.php';
include_once 'config/connect_db.php';

define('ACTION_LOGIN', 'ACTION_LOGIN');
define('ACTION_LOGOUT', 'ACTION_LOGOUT');


if ($_POST['action'] == ACTION_LOGIN) {

    $username = $_POST['username'];
    $password = $_POST['password'];

    $prepare = $pdo->prepare(
        'SELECT users.*, ud.full_name
            FROM users INNER JOIN userdetails ud on users.name = ud.name
            WHERE ud.name = ? LIMIT 1'
    );
    $prepare->execute([$username]);
    $res = $prepare->fetch();

    if (!$res) {

        header("HTTP/1.1 404 Not Found");
        echo json_encode(
            ['message' => 'Tên đăng nhập không tồn tại'], JSON_UNESCAPED_UNICODE
        );

    } else {
        if (!password_verify($password, $res['hashed_password'])) {

            header("HTTP/1.1 401 Unauthorized");
            echo json_encode(
                ['message' => 'Sai mật khẩu'], JSON_UNESCAPED_UNICODE
            );

        } else {

            $_SESSION[USER_NAME] = $res['name'];
            $_SESSION[FULL_NAME] = $res['full_name'];
            $_SESSION[USER_TYPE] = $res['is_admin'] ? ADMIN : NORMAL_USER;

            header('HTTP/1.1 200 OK');
            echo json_encode(
                ['message' => 'Đăng nhập thành công',
                    'is_admin' => $res['is_admin']], JSON_UNESCAPED_UNICODE
            );

        }
    }
} else {
    if ($_POST['action'] == ACTION_LOGOUT) {

        unset($_SESSION[USER_NAME]);
        unset($_SESSION[USER_TYPE]);
        unset($_SESSION[FULL_NAME]);

        header('HTTP/1.1 200 OK');
        echo 'Logout successfully';
    }
}
